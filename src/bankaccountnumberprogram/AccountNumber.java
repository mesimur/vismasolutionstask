/* Henna Pekkala, 2017 
 * for Visma Solutions Aoplication Process - Round 1
 * The task was to design and implement traditional 14-character based
 * bank account number class and a small "client-end".
 *
 * AccountNumber class
 */

package bankaccountnumberprogram;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AccountNumber {
     private String shortFormat;
     private String longFormat;
     private String bankingGroup;
     // numberconversion toshort? liian lyhyt? 6-2(-8)? liikaa -? kaikkien tarkistusten jälkeen short ja long, pankki lukemisessa
     private ArrayList<String> bankingGroupsNumber = new ArrayList<>();
     private ArrayList<String> bankingGroupsName = new ArrayList<>();
     //The file that has the info of the numbers that represent different banking groups.
     private String filename = "BankingGroups.txt";

     
     public AccountNumber(String s) throws FileNotFoundException, IOException{
       //Reads the file that contains the info of banking groups.
        readFile(filename);
        
        //Check if the field was empty
        if (s.trim().isEmpty()){
            throw new IllegalArgumentException("The field is empty.\n");
        } 
        
        //Check if input is too long
        else if(s.replace("-", "").length() > 14){
            throw new IllegalArgumentException("Account number is too long.\n");
        } 
        
        //Check if input contains only numbers (and one dash).
        else if (containsOnlyNumbers(s) == false){
            throw new IllegalArgumentException("Account number must contain only numbers and at most only one dash.\n");
        }
        
        //Check if input is of format xxxxxx(-)xx(xxxxxx).
        else if(rightFormat(s) == false){
            throw new IllegalArgumentException("Account number's first part should consist of six digits and the second part of 2-8 digits.\n");
        }

        //Check if input starts with anumber that indicates the banking group.
        else if (startsCorrectly(s) == false){
            throw new IllegalArgumentException("Account number does not start correctly.\n");
        }
        
        //Check that the last digit is valid.
        else if(calculateCheckDigit(s) != Character.getNumericValue(s.charAt(s.length()-1))){
            throw new IllegalArgumentException("Check digit is not valid.\n");
        }
        
        //If everything checks out, we can create the account number object.
        else{
            shortFormat = shortFormatConversion(s);
            longFormat = longFormatConversion(s); 
            int i;
            //Let's search what banking group the number belongs to
            for (i = 0; i < bankingGroupsNumber.size(); i++){
                if (s.startsWith(bankingGroupsNumber.get(i))){
                    break;
                }
            }
            bankingGroup = bankingGroupsName.get(i); 
        }
    }
     
     //This function reads the file that contains info of the numbers that indicates the banking group.
     private void readFile(String fn) throws FileNotFoundException, IOException{
         BufferedReader in = new BufferedReader(new FileReader(fn));
         String line = null;
         String[] lineSplit;
         
         //The format of the info is "number=bank"
         while ((line = in.readLine()) != null) {
             lineSplit = line.split("=");
             //Let's save numbers and names to different array lists.
             bankingGroupsNumber.add(lineSplit[0]);
             bankingGroupsName.add(lineSplit[1]);
         }
         in.close();
     }
     
     //This function checks that the input contains only numbers. Returns a boolean.
     private boolean containsOnlyNumbers(String s){
        boolean cON = false;
        //There should be only one dash, so let's just replace the first instance of it.
        s = s.replaceFirst("-", "");
        
        //This loop checks every character. If a character is not a digit the loop breaks.
        for (int i = 0; i < s.length(); i++){
            cON = Character.isDigit(s.charAt(i));
            if (cON == false){
                break;
            }
        }
        return cON;
    }
     
     //This function checks that the input is in format xxxxxx(-)xx(xxxxxx).
     private boolean rightFormat(String s){
         boolean rF = false;
         String[] parts = s.split("-");
         
         //If the input contains a dash we have to check that the first part is
         //six digits long and the other part consists of 2-8 digits.
         if (s.contains("-")){
             if (parts[0].length() == 6 && parts[1].length() >= 2 && parts[1].length() <= 8){
                 rF =true;
             }
         } 
         
         //If the input does not contain a dash we have to check that it has at least 
         //8 characters.
         else{
             if (s.length() >= 8){
                rF =true;
             }
         }
         return rF;
     }
         
     //This function makes sure the input starts correctly.
     private boolean startsCorrectly(String s){
        boolean sC = false;
        
        //The start of the input must be a digit that indicates the banking group.
        for (String i : bankingGroupsNumber){
            if (s.startsWith(i)){
                sC = true;
                break;
            }
        }
        return sC;
     }
     
    //This function converts the number from short format to the electronic account number.
     private String longFormatConversion(String s){
        String eNumber;
        s = s.replace("-", "");
        
        //If the account number starts with 4 or 5 it can be transformed to the
        //long format by adding zeros after the seventh digit from the left.
        if (s.startsWith("4") || s.startsWith("5")){
            //The first seven numbers
            eNumber = s.substring(0,7);
            
            //And then as much zeros as can be fitted in 14 digits
            for (int j = 0; j < (14-s.length()); j++){
                eNumber += 0;
            }
            //And then the last digits.
            eNumber += s.substring(7, s.length());
        }
        //If the account number does not start with 4 or 5 it can be transformed to the
        //long format by adding zeros after the sixth digit from the left.
        else{
            eNumber = s.substring(0,6);
            
            for (int k = 0; k < (14-s.length()); k++){
                eNumber += 0;
            }
            eNumber += s.substring(6, s.length());
        }  
        return eNumber;
     }
     
     //This function calculates the last digit using the weighted coefficients
     //of Luhn Modulus 10 with weights 2, 1, 2, 1,... from right to left.
     private int calculateCheckDigit(String num){
         //sum is the final sum of the calculations
         int sum  = 0;
         //n is the number at the index i
         int n;
         //LastDigit is the calculated check digit.
         int lastDigit;
         //The account number must be in long format for this.
         num = longFormatConversion(num);
         
         //Let's calculate the numbers one by one.
         for (int i = 0; i < num.length()-1; i++){
            n = Character.getNumericValue(num.charAt(i));
            
            //The 13th number's index is 12 (indexes start from zero).Therefore 
            //every index that is divisible by two must be multiplied with two.
            if (i % 2 == 0) { 
                n = n*2;
                
                //If the result consists of two digits they must be added up to 
                //get a single digit.
                if (n > 9){
                    n = (n % 10) + 1;
                }
            }
            
            //Then we can add the calculated result to the sum.
             sum += n;  
         }
         
         //Last digit can be calculated by deducting the sum from the next multiple of 10.
         lastDigit = 10 - sum % 10;
         return lastDigit;
     }
     
     //We can get the short format by removing the zeros and adding a dash.
     private String shortFormatConversion(String s){
        String shortNumber;
        int i;
        //Remove the zeros after the first six/seven digits depending on the bank.
        if (s.startsWith("4") || s.startsWith("5")){
            i = 7;
        }
        
        else{
            i = 6;
        }
        
        shortNumber = s.substring(0,i);
        
        //The program goes past the zeros.
        while (s.charAt(i) == '0'){
            i++;
        }
        //Add the rest after zeros.
         shortNumber += s.substring(i,s.length());
         
        //Add the dash if it's not there already
        if (!shortNumber.contains("-")){
            shortNumber =  shortNumber.substring(0,6) + "-" + shortNumber.substring(6, shortNumber.length());
        }
        return shortNumber;
     }
     
     //Getter for account number's short format.
    public String getShortFormat() {
        return shortFormat;
    }
    
    //Getter for account number's long format.
    public String getLongFormat() {
        return longFormat;
    }

    //Getter for account number's banking group.
    public String getBankingGroup() {
        return bankingGroup;
    }
}
