/* Henna Pekkala, 2017 
 * for Visma Solutions Aoplication Process - Round 1
 * The task was to design and implement traditional 14-character based
 * bank account number class and a small "client-end".
 *
 * Main
 */

package bankaccountnumberprogram;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BankAccountNumberProgram {

    public static void main(String[] args) {
        //Scanner for user input
        Scanner scan = new Scanner(System.in);
        //User's choice. First it is one so we can get in to the while-loop
        int choice = 1;
        //Inputted account number
        String inputNumber = null;
        //Account number object
        AccountNumber aN = null;
        
        //The menu
        while(choice != 0){
            
            System.out.print("*** Bank account number ***\n"
                            + "1) Input a new account number\n"
                            + "2) Inspect the last valid account number\n"
                            + "0) Quit\n"
                            + "Your choice: ");
            
            //Try-catch is here incase user inputs a string.
            try{
                choice = scan.nextInt();
                switch(choice){
                    
                    //If user choose number one the program asks for an account 
                    //number and tries to create an object.
                    case(1):
                        System.out.print("\nThe account number must be input in format"
                                + " xxxxxx(-)xx(xxxxxx)\nand must consist only from numbers.\n"
                                + "Account number: " );
                        inputNumber = scan.next();
                        
                        //Try to create an object. It will not be possible
                        //if the input number is not valid.
                        try {
                            aN = new AccountNumber(inputNumber);
                            System.out.println("Account number was valid!\n");
                            
                        } catch (IllegalArgumentException exception) {
                            System.out.println(exception.getMessage());
                        } catch (IOException exception) {
                             System.out.println(exception.getMessage());
                        } 
                        break;
                    
                    //Here the program shows info about the inputted number.
                    case(2):
                        //Check that the object has been created successfully.
                        if (aN != null){
                            System.out.println("The account number in short format: " 
                                    + aN.getShortFormat() + "\n"
                                    + "The account number in long format: " 
                                    + aN.getLongFormat() + "\n"
                                    + "The account number's banking group: " 
                                    + aN.getBankingGroup() + "\n");
                        }
                        //And if not.
                        else{
                            System.out.println("A valid account number has not been input.\n");
                        }
                        break;
                    
                    //This quits the program. 
                    case(0):
                        System.out.println("Thank you!");
                        break;
                        
                    //This is for wrong int type inputs
                    default:
                        System.out.println("Try again.\n");
                }
                
            } catch (InputMismatchException exc){
                System.out.println("Try again.\n");
                //Scan.next() is here to prevent infinite loops.
                scan.next();
            }
        }  
    }
}
